package com.devcamp.s20.midtest1.stringapi.service;

import org.springframework.stereotype.Service;

@Service
public class ConcatenationService {
     public String concatenateString(String input1, String input2){
          int length1 = input1.length();
          int length2 = input2.length();

            // Cắt bỏ các ký tự đầu của chuỗi dài hơn cho đến khi độ dài bằng nhau
          while (length1 > length2) {
               input1 = input1.substring(1);
               length1--;
          }

          while (length2 > length1) {
               input2 = input2.substring(1);
               length2--;
          }

          // Gắn chúng lại với nhau
          return input1.concat(input2);
     }
}
