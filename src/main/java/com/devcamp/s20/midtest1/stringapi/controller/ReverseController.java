package com.devcamp.s20.midtest1.stringapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s20.midtest1.stringapi.service.ReverseService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ReverseController {
     private ReverseService reverseService;

     public ReverseController(ReverseService reverseService){
          this.reverseService = reverseService;
     }

     @GetMapping("/reverse")
     public String reverseString(@RequestParam String input){
          return reverseService.reverseString(input);
     }
}
