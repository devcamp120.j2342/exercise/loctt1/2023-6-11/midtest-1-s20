package com.devcamp.s20.midtest1.stringapi.models;

public class ConcatenationRequest {
     private String input1;
     private String input2;

     public ConcatenationRequest() {
     }

     public ConcatenationRequest(String input1, String input2) {
          this.input1 = input1;
          this.input2 = input2;
     }

     public String getInput1() {
          return input1;
     }

     public void setInput1(String input1) {
          this.input1 = input1;
     }

     public String getInput2() {
          return input2;
     }

     public void setInput2(String input2) {
          this.input2 = input2;
     }
}
