package com.devcamp.s20.midtest1.stringapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.devcamp.s20.midtest1.stringapi.service.ConcatenationService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ConcatenationController {
     private ConcatenationService concatenationService;

     public ConcatenationController(ConcatenationService concatenationService){
          this.concatenationService = concatenationService;
     }

     @GetMapping("/concatenate")
     public String concatenateString(@RequestParam("input1") String input1, @RequestParam("input2") String input2){
          return concatenationService.concatenateString(input1, input2);
    }
}

