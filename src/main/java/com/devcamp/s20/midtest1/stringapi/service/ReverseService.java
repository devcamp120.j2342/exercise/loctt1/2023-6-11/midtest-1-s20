package com.devcamp.s20.midtest1.stringapi.service;

import org.springframework.stereotype.Service;

@Service
public class ReverseService {
     public String reverseString(String input){
          StringBuilder reversed = new StringBuilder(input);
          reversed.reverse();
          return reversed.toString();
     }
}
