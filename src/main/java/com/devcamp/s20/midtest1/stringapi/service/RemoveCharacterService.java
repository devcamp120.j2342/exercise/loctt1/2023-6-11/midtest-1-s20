package com.devcamp.s20.midtest1.stringapi.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

@Service
public class RemoveCharacterService {
     public String RemoveCharacter(String input){
          StringBuilder result = new StringBuilder();
          Set<Character> seen = new HashSet<>();

          for(char ch : input.toCharArray()){
               if(seen.add(ch)){
                    result.append(ch);
               }
          }
          return result.toString();
     }     
}
