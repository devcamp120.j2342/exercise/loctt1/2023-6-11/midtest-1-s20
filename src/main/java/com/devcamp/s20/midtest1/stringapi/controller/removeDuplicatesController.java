package com.devcamp.s20.midtest1.stringapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s20.midtest1.stringapi.service.RemoveCharacterService;




@RestController
@CrossOrigin
@RequestMapping("/")
public class removeDuplicatesController {
     
     private RemoveCharacterService removeCharacterService;
    

     public removeDuplicatesController(RemoveCharacterService removeCharacterService){
          this.removeCharacterService = removeCharacterService;
     }

     @GetMapping("/removeDuplicates")
     public String removeDuplicates(@RequestParam String input){
          return removeCharacterService.RemoveCharacter(input);
     }
}
