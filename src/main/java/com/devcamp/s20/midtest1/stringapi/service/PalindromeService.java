package com.devcamp.s20.midtest1.stringapi.service;

import org.springframework.stereotype.Service;

@Service
public class PalindromeService {
      public boolean isPalindrome(String input){
          String reversed = new StringBuilder(input).reverse().toString();
          return input.equals(reversed);
      }
}
