package com.devcamp.s20.midtest1.stringapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s20.midtest1.stringapi.service.PalindromeService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class PalindromeController {
    private PalindromeService palindromeService;

    public PalindromeController(PalindromeService palindromeService){
     this.palindromeService = palindromeService;
    }

     @GetMapping("/palindrome")
     public String checkPalindrome(@RequestParam String input){
          if(palindromeService.isPalindrome(input)){
               return "Chuỗi đầu vào là Palindrome!!!";
          }
          else{
               return "Chuỗi đầu vào không phải là Palindrome";
          }
     }
}
